/*****Requeridos*****/
const express = require('express');
const app = express();
const logger = require('./utils/log.js');
require('dotenv').config();
//configura la decodifcacion y codificacion de json para los rest
app.use(express.urlencoded({ extended: true }));
app.use(express.json({ limit: '20mb' }));
/*****Requeridos*****/
const compression = require('compression');
const config = require('./config.js');
const routes = require('./router.js');
const mysqldb = require('./mysql.js');
// Default router = express;
routes.default(app);
// archivos estaticos)
// app.use('/public', express.static(__dirname + '/public'));
/**** compresion de peticiones */
const shouldCompress = (req, res) => {
    if (req.headers['x-no-compression']) {
        // No comprimira las respuestas, si este encabezado 
        // está presente.
        return false;
    }
    // Recurrir a la compresión estándar
    return compression.filter(req, res);
};
/***************** */
app.use(compression({
    // filter: Decide si la respuesta debe comprimirse o no,
    // en función de la función 'shouldCompress' anterior
    filter: shouldCompress,
    // threshold: Es el umbral de bytes para el tamaño del cuerpo
    // de la respuesta antes de considerar la compresión,
    // el valor predeterminado es 1 kB
    threshold: 0
}));
// establecer Server
app.set('superSecret', config.secret);

function StartServer() {
    app.listen(config.server.port, function() {
        logger.info('Server app listening on port ' + process.env.PORT);
        //var ruta=require('./models/users/model.users');
    });
}

mysqldb.getConnection(function(err) {
    if (err) {
        return console.error('error: ' + err.message);
    }
    StartServer();
    logger.info('Connected to the MySQL server.');
});

module.exports = app;