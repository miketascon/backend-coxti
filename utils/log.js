const { createLogger, format, transports } = require('winston');
const moment = require('moment');
let datePrint = moment(new Date()).format('YYYY-MM-DD') + ' ' + moment(new Date()).format('hh:mm:ss');
/*************************
 * generación de logs
 *************************/

module.exports = createLogger({
    format: format.combine(
        format.simple(),
        format.timestamp(),
        format.printf(info => `[${datePrint}] ${info.level}: ${info.message}`)
    ),
    transports: [
        new transports.File({
            maxsize: 5120000,
            maxFiles: 5,
            filename: `${__dirname}/../logs/log.log/`

        }),
        new transports.Console({
            level: 'debug',
        })
    ]
});