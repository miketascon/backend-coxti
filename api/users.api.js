const express = require('express');
const controller = require('../controllers/users.controller');
const { verificaToken } = require('../middlewares/authentication');

const router = express.Router();

router.post('/login', controller.login);
router.post('/', controller.create);
// acciones con validacion de token
router.get('/', verificaToken, controller.findById);
router.put('/', verificaToken, controller.update);
router.get('/aleatorio', controller.aleatorio);
router.put('/updatepass', verificaToken, controller.updatePass);
router.delete('/:id', verificaToken, controller.delete);

module.exports = router;