# Instalación de dependencias
npm install

# Scripts
npm test -> nodemon server.js

npm start -> node server.js

# Dependencias 
"compression": "^1.7.4",
"crypto": "^1.0.1",
"dotenv": "^8.2.0",
"express": "^4.17.1",
"jsonwebtoken": "^8.5.1",
"moment": "^2.29.1",
"mysql": "^2.18.1",
"winston": "^3.3.3"

# Dependencias de desarrollo

"nodemon": "^2.0.7"

# Base de datos
MySql
El archivo para la generación de la DB esta incluido dentro del proyecto

# Descripción
Este es una API REST de node.js creado con express.

Se creo el api con una estructura de carpetas simple separando las rutas, los controladores, middlewares peronalizados y funciones.

En el primer nivel se encuentran los archivos necesarios para su funcionamiento, la entrada es el server.js y en el archivo router.js se encuentra el manejo de los CORS.

También se separaron las variables de entorno y configuración en un archivo .env, la configuración del server esta en el archivo config.js y de la base datos en mysql.js.


# Observación
La prueba fue construido con exppress y no con adonis puesto que el tiempo que dispuse para hacerla era limitado y no he llegado a utilizar el frameword de adonis anteriormente, entonces para no complicarme por el momento se desarrollo con express. Ya estoy viendo la documentación oficial de adonis y como funciona el framework.





