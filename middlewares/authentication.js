const jwt = require('jsonwebtoken');
const logger = require('../utils/log.js');

// =====================
// Verificar Token
// =====================
let verificaToken = (req, res, next) => {
    let token = req.get('token');
    jwt.verify(token, process.env.SEED, (err, decoded) => {
        if (err) {
            logger.info(err);
            return res.json({
                ok: false,
                message: 'Token no válido'
            });
        }
        req.usuario = decoded.usuario;
        next();
    });
};

module.exports = {
    verificaToken
};