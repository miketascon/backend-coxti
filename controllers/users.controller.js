const jwt = require('jsonwebtoken');
const moment = require('moment');
const db = require('../mysql');
const encrypt = require('../functions/encrypt.js');
const aleatorio = require('../functions/ramdon.js');
const logger = require('../utils/log.js');

/***Login de usuario */

module.exports.login = (req, res) => {
    const pass = encrypt.encriptar(process.env.SECRET, req.body.password);
    db.query('SELECT id, nombres, apellidos, email FROM `users` WHERE ' +
        `email="${req.body.email}" AND password="${pass}"`, (err, results) => {
            if (err) {
                logger.info(err);
                res.json({
                    err,
                    message: 'error interno',
                    success: false
                });
            } else {
                if (results[0]) {
                    let token = jwt.sign({
                        usuario: results
                    }, process.env.SEED, { expiresIn: process.env.CADUCIDAD_TOKEN });
                    res.json({
                        results,
                        token,
                        message: 'login',
                        success: true
                    });
                } else {
                    res.json({
                        message: 'Usuario o contraseña incorrecta',
                        success: false
                    });
                }
            }
        });
};


/*****Buscar usuario por ID */

module.exports.findById = (req, res) => {

    db.query('SELECT id, nombres, apellidos, email, identificacion, celular, ' +
        'departamento, ciudad, barrio, direccion, salario, otros_ingresos, ' +
        'gastos_mensuales, gastos_financieros FROM `users` WHERE ' +
        `id="${req.query.id}"`, (err, results) => {
            if (err) {
                logger.info(err);
                res.json({
                    err,
                    message: 'error interno',
                    success: false
                });
            } else {
                if (results[0]) {
                    res.json({
                        results,
                        message: 'query user id',
                        success: true
                    });
                } else {
                    res.json({
                        message: 'registro no encontrado',
                        success: false
                    });
                }
            }
        });


};

/***Creación de usuario */
module.exports.create = (req, res) => {
    const pass = encrypt.encriptar(process.env.SECRET, req.body.password);
    const createAt = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
    db.query('INSERT INTO `users` (nombres, apellidos, email, password, create_at) ' +
        `VALUES ("${req.body.nombres}", "${req.body.apellidos}", "${req.body.email}", "${pass}", "${createAt}");`,
        function(err, result) {
            if (err) {
                logger.info(err);
                res.json({
                    err,
                    message: 'error interno',
                    success: false
                });
            } else {
                res.json({
                    result,
                    message: 'operación exitosa',
                    success: true
                });
            }
        });
};

/***Actualización de usuario */

module.exports.update = (req, res) => {
    db.query("UPDATE `users` SET " +
        `nombres="${req.body.user.nombres}", apellidos="${req.body.user.apellidos}", ` +
        `identificacion="${req.body.user.identificacion}", celular="${req.body.user.celular}", ` +
        `departamento="${req.body.user.departamento}", ciudad="${req.body.user.ciudad}", ` +
        `barrio="${req.body.user.barrio}", direccion="${req.body.user.direccion}", ` +
        `salario="${req.body.user.salario}", otros_ingresos="${req.body.user.otros_ingresos}", ` +
        `gastos_mensuales="${req.body.user.gastos_mensuales}", gastos_financieros="${req.body.user.gastos_financieros}" ` +
        `WHERE id = ${req.query.id}`,
        (err, results) => {
            if (err) {
                logger.info(err);
                res.json({
                    err,
                    message: 'error interno',
                    success: false
                });
            } else {
                res.json({
                    results,
                    message: 'operación exitosa',
                    success: true
                });
            }
        });
};


/*****Actualizar contraseña*/

module.exports.updatePass = (req, res) => {
    const pass = encrypt.encriptar(process.env.SECRET, req.body.password);
    db.query("UPDATE `users` SET " +
        `password="${pass}" WHERE id=${req.query.id}`,
        (err, results) => {
            if (err) {
                logger.info(err);
                res.json({
                    err,
                    message: 'error interno',
                    success: false
                });
            } else {
                res.json({
                    results,
                    message: 'operación exitosa',
                    success: true
                });
            }
        });
};


/***Eliminacion de usuario */

module.exports.delete = (req, res) => {
    db.query("DELETE FROM `users` " +
        `WHERE id=${req.params.id}`,
        (err, results) => {
            if (err) {
                logger.info(err);
                res.json({
                    err,
                    message: 'error interno',
                    success: false
                });
            } else {
                res.json({
                    results,
                    message: 'operación exitosa',
                    success: true
                });
            }
        });
};


/*********Generación de rangos aleatorios ******/

module.exports.aleatorio = (req, res) => {

    let min = Number(req.query.number) / 2;
    let max = Number(req.query.number) * 2;
    // primer rango
    let a = aleatorio.ramdonInverse(min, max);
    let b = a + max;
    // segundo rango
    let c = a + max / 2;
    let d = a - max / 2;
    // tercer rango incluido el valor dado
    let e = Number(req.query.number) + a;
    let f = a / 2 - Number(req.query.number);

    let caso = aleatorio.ramdon(1, 6);

    // case aleatorio
    switch (caso) {
        case 1:
            res.json({
                a: `${a} - ${b}`,
                b: `${d - 25} - ${c + 26}`,
                c: `${Math.round(f) + 28} - ${e + 28}`
            });
            break;
        case 2:
            res.json({
                a: `${a} - ${b}`,
                b: `${Math.round(f) + 28} - ${e + 28}`,
                c: `${d - 25} - ${c + 26}`
            });
            break;
        case 3:
            res.json({
                a: `${d - 25} - ${c + 26}`,
                b: `${Math.round(f) + 28} - ${e + 28}`,
                c: `${a} - ${b}`
            });
            break;
        case 4:
            res.json({
                a: `${d - 25} - ${c + 26}`,
                b: `${a} - ${b}`,
                c: `${Math.round(f) + 28} - ${e + 28}`
            });
            break;
        case 5:
            res.json({
                a: `${Math.round(f) + 28} - ${e + 28}`,
                b: `${a} - ${b}`,
                c: `${d - 25} - ${c + 26}`
            });
            break;
        case 6:
            res.json({
                a: `${Math.round(f) + 28} - ${e + 28}`,
                b: `${d - 25} - ${c + 26}`,
                c: `${a} - ${b}`
            });
            break;

        default:
            res.json({
                a: `${a} - ${b}`,
                b: `${d - 25} - ${c + 26}`,
                c: `${Math.round(f) + 28} - ${e + 28}`,
            });
    }
};