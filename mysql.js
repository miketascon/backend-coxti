const mysql = require('mysql');

/****pool de conexiones */
const mysqldb = mysql.createPool({
    host: process.env.MYSQLHOST,
    user: process.env.MYSQLUSER,
    password: process.env.MYSQLPASSWORD,
    database: process.env.MYSQLDATABASE,
    connectionLimit: 100,
    waitForConnections: true,
    queueLimit: 0,


});

module.exports = mysqldb;