/***
 * Configuracion del services
 */

const config = {
    server: {
        hots: process.env.HOST,
        port: process.env.PORT
    },
    secret: process.env.SECRET
};

module.exports = config;